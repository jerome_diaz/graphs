package fr.jdiaz.graphs;

public abstract class AbstractEdge<E> implements Edge<E> {

	private final E mEndA;
	private final E mEndB;
	
	public AbstractEdge(E pEndA, E pEndB) {
		mEndA = pEndA;
		mEndB = pEndB;
	}
	
	@Override
	public E getEndANodeId() {
		return mEndA;
	}
	
	@Override
	public E getEndBNodeId() {
		return mEndB;
	}
	
}
