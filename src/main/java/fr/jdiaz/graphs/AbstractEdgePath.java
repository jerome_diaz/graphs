package fr.jdiaz.graphs;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractEdgePath<E> implements EdgePath<E> {
	
	private final List<Edge<E>> mEdgePath;
	
	public AbstractEdgePath() {
		mEdgePath = new ArrayList<Edge<E>>();
	}

	@Override
	public List<Edge<E>> edges() {
		return mEdgePath;
	}
	
}
