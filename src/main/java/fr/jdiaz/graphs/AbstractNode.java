package fr.jdiaz.graphs;

public abstract class AbstractNode<E> implements Node<E> {

	private final E mId;
    
	public AbstractNode(E pId)  {
		mId = pId;
	}
	
	@Override
	public E getNodeId() {
		return mId;
	}
}
