package fr.jdiaz.graphs;

import java.util.Collection;

public interface Edge<E> {

    public E getEndANodeId();
    public E getEndBNodeId();
    
}
