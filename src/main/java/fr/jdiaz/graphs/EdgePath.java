package fr.jdiaz.graphs;

import java.util.*;

public interface EdgePath<E> {

    public List<Edge<E>> edges();
    
}