package fr.jdiaz.graphs;

import java.util.*;

public interface Node<E> {

    public E getNodeId();
}